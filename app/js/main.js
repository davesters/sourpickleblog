SP.Main = (function () {

  var postPreview = $('#posts-list-preview');

  var onPostClick = function() {
    var type = $(this).data('type');
    var title = $(this).find('h3').text();
    var html = markdown.toHTML($(this).find('.post-body').val());
    
    html = '<h1>' + title + '</h1>' + html;
    $(postPreview).html(html);
    
    $('#edit-post').attr('href', '/admin/' + type + '/' + $(this).data('id'));
    $('#edit-post').removeClass('hide');
  };
  
  return {
    init: function () {
      if ($('.posts-list').length > 0) {
        $('.post').on('click', onPostClick);
      }

      if ($('#edit-page').length > 0) {
        SP.EditPostView.init();
      }

      if ($('#settings-page').length > 0) {
        SP.Settings.init();
      }
    }

  };
} ());

$(SP.Main.init());