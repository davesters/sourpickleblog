SP.Settings = (function () {

  var onTabClick = function(e) {
      e.preventDefault();
      var tab = $(this).data('tab');
      
      $('.settings-menu').find('li').removeClass('active');
      $('.settings-tab').addClass('hide');
      
      $('#' + tab).removeClass('hide');
      $(this).parent().addClass('active');
      
      return false;
  };
  
  return {
    init: function () {
      $('.settings-menu').find('a').on('click', onTabClick);
    }

  };
} ());