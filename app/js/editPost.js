SP.EditPostView = (function() {
  
  var postPreview = $('#post-preview');
  var postEditor = $('#post-editor');
  var postTitle = $('#post-title');
  var postId = $('#post-id');
  var postType = $('#post-type').val();
  var saving = false;
  var timezone = moment.tz.guess();
  var scrollTimer;

  var renderPreview = function() {
    var html = markdown.toHTML($(postEditor).val());
    html = '<h1>' + $(postTitle).val() + '</h1>' + html;

    $(postPreview).html(html);
  };
  
  var savePost = function(publish) {
    saving = true;

    var pubDate = moment($('#post-published').val());
    
    $.ajax({
      url: '/admin/' + postType + '/' + $(postId).val(),
      method: 'POST',
      dataType: 'json',
      data: {
        title: $(postTitle).val(),
        body: $(postEditor).val(),
        slug: $('#post-slug').val(),
        published: pubDate.utc().format('MM/DD/YYYY HH:mm'),
        inMenu: $('#InMenu').is(":checked"),
        isPublished: publish,
      },
      success: function(data) {
        if (!data.success) {
          saving = false;
          // TODO: Show error
        }
        
        window.location.href = '/admin/' + postType + '/' + data.id;
      },
      error: function() {
          saving = false;
        // TODO: Show error
      },
    });
  };
  
  var onSavePost = function(e) {
    e.preventDefault();
    if (saving) {
      return false;
    }
    
    savePost(false);
    return false;
  };

  var onPublishPost = function(e) {
    e.preventDefault();
    if (saving) {
      return false;
    }
    
    savePost(true);
    return false;
  };
  
  var onSettingsShow = function(e) {
    e.preventDefault();
    $('#post-settings').removeClass('hide');
    return false;
  };

  var onSettingsHide = function(e) {
    e.preventDefault();
    $('#post-settings').addClass('hide');
    return false;
  };
  
  var onScroll = function(mirrorElem) {
    mirrorElem.unbind('scroll');
    
    var percentage = this.scrollTop() / (this[0].scrollHeight - this[0].offsetHeight);
    var x = percentage * (mirrorElem[0].scrollHeight - mirrorElem[0].offsetHeight);

    mirrorElem.scrollTop(x);
    
    if (typeof(scrollTimer) !== 'undefind') {
      clearTimeout(scrollTimer);
    }
    
    scrollTimer = setTimeout(function() {
      mirrorElem.on('scroll', onScroll.bind(mirrorElem, this));
    }.bind(this), 300);
  };
  
  return {

    init: function () {
      if (postEditor.length === 0) {
        return;
      }

      $(postEditor).on('scroll', onScroll.bind(postEditor, postPreview));
      $(postPreview).on('scroll', onScroll.bind(postPreview, postEditor));
      
      $(postEditor).on('input', renderPreview);
      $(postTitle).on('input', renderPreview);
      
      $('#save-post').on('click', onSavePost);
      $('#publish-post').on('click', onPublishPost);
      
      $('#open-settings').on('click', onSettingsShow);
      $('#close-settings').on('click', onSettingsHide);
      
      var pubDate = moment.utc($('#post-published').val());
      pubDate = pubDate.tz(timezone);
      $('#post-published').val(pubDate.format('MM/DD/YYYY HH:mm'));
      
      $('#post-published').fdatepicker({
        format: 'mm/dd/yyyy hh:ii',
        disableDblClickSelection: true,
        pickTime: true
      });
      
      renderPreview();
    }

  };
  
}());