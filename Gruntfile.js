"use strict";

let jsFiles = [
  'app/js/_lib/moment-with-locales.js',
  'app/js/_lib/moment-timezone-with-data.js',
  'app/js/_lib/foundation.js',
  'app/js/_lib/foundation-datepicker.js',
  'app/js/_lib/markdown.js',
  'app/js/editPost.js',
  'app/js/settings.js',
  'app/js/main.js',
];

module.exports = function (grunt) {

  grunt.initConfig({

    uglify: {
      dev: {
        options: {
          sourceMap: true,
          mangle: false,
          compress: false,
          preserveComments: true
        },
        files: {
          'wwwroot/js/main.min.js': jsFiles,
        }
      },
      prod: {
        options: {
          sourceMap: false,
          mangle: false,
          compress: false,
          preserveComments: false
        },
        files: {
          'wwwroot/js/main.min.js': jsFiles,
        }
      }
    },

    cssmin: {
      prod: {
        options: {
          expand: true,
          report: 'min',
          keepBreaks: true,
          noAdvanced: true
        },
        files: {
          'wwwroot/css/main.min.css': [
            'app/css/normalize.css',
            'app/css/foundation.css',
            'app/css/foundation-datepicker.css',
            'app/css/font-awesome.css',
            'app/css/main.css',
          ]
        }
      }
    },

    jshint: {
      files: [ './app/js/*.js', './Gruntfile.js' ],
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: 'nofunc',
        undef: false,
        unused: true,
        validthis: true,
        esnext: true,
        node: true,
        laxbreak: true,
        globals: {
          jQuery: true,
          require: true
        },
        devel: {
          console: true
        }
      }
    },
    
    shell: {
      startAllContainers: {
        command: 'docker-compose up -d',
        options: {
          canKill: false,
          failOnError: false
        }
      },
      stopAllContainers: {
        command: 'docker-compose stop',
        options: {
          canKill: false,
          failOnError: false
        }
      },
      restartContainer: {
        command: 'docker-compose restart web',
        options: {
          canKill: false,
          failOnError: false
        }
      }
    },

    watch: {
      web: {
        files: ['src/**/*.cs'],
        tasks: ['shell:restartContainer']
      },
      css: {
        files: 'app/css/*.css',
        tasks: ['cssmin']
      },
      js: {
        files: 'app/js/**/*.js',
        tasks: ['uglify:dev']
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-shell-spawn');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint', 'uglify:dev', 'cssmin', 'shell:startAllContainers', 'watch']);
  grunt.registerTask('build', ['uglify:prod', 'cssmin']);
  grunt.registerTask('stop', ['shell:stopAllContainers']);

};