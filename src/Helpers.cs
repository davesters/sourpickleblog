using System;
using System.Text.RegularExpressions;
using JWT;
using Nancy;
using Newtonsoft.Json.Linq;
using NodaTime;
using SourPickle.Models;

namespace SourPickle
{
    public static class Helpers
    {
        private static string _secret = Environment.GetEnvironmentVariable("SP_SECRET");

        public static Response CheckAuth(NancyContext context)
        {
            if (!context.Request.Cookies.ContainsKey("SP")) {
                return null;
            }
            
            var token = context.Request.Cookies["SP"];
            try
            {
                var json = JObject.Parse(JsonWebToken.Decode(token, _secret));
                context.CurrentUser = new User {
                    UserName = (string)json["user"],
                };
            }
            catch (JWT.SignatureVerificationException)
            {
                return null;
            }
            
            return null;
        }
        
        public static string GetPrettyDate(DateTime d)
        {
            var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
            if (d > now) {
                return "On " + d.ToString();
            }
            
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "just now";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minute ago";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minutes ago",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} hours ago",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago",
                dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago",
                Math.Ceiling((double)dayDiff / 7));
            }
            if (dayDiff < 365)
            {
                return string.Format("{0} months ago",
                Math.Ceiling((double)dayDiff / 30));
            }
            if (dayDiff == 365)
            {
                return "1 year ago";
            }
            if (dayDiff > 365 && dayDiff < 720)
            {
                return "over 1 year ago";
            }
            if (dayDiff > 720)
            {
                return string.Format("over {0} years ago",
                Math.Floor((double)dayDiff / 365));
            }
            return null;
        }
        
        public static string SlugifyText(string txt)
        {
            if (string.IsNullOrWhiteSpace(txt))
            {
                return string.Empty;
            }

            string slug = RemoveAccent(txt).ToLower();
            slug = Regex.Replace(slug, @"[^a-z0-9\s-]", ""); // invalid chars
            slug = Regex.Replace(slug, @"\s+", " ").Trim(); // convert multiple spaces into one space
            slug = slug.Substring(0, slug.Length <= 62 ? slug.Length : 62).Trim(); // cut and trim it
            slug = Regex.Replace(slug, @"\s", "-"); // hyphens
            slug = Regex.Replace(slug, @"(-)+", "-"); // remove extra duplicate hyphens

            return slug;
        }
        
        private static string RemoveAccent(string txt)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }
    }
}