using Microsoft.AspNet.Builder;
using Microsoft.AspNet.FileProviders;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.StaticFiles;
using Microsoft.Extensions.PlatformAbstractions;
using Nancy;
using Nancy.Owin;

namespace SourPickle
{ 
    public class Startup
    {
        public static string AppBasePath = "";
        
        public void Configure(IApplicationBuilder app, IApplicationEnvironment appEnv)
        {
            AppBasePath = appEnv.ApplicationBasePath;
            
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppBasePath + "/wwwroot"),
                RequestPath = new PathString("/admin")
            });
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(AppBasePath + "/themes/default"),
                RequestPath = new PathString("")
            });
            app.UseOwin(x => x.UseNancy());
        }

        // Entry point for the application.
        public static void Main(string[] args)
        {
            WebApplication.Run<Startup>(args);
        }
    }
    
    public class CustomRootPathProvider : IRootPathProvider
    {
        public string GetRootPath()
        {
            return Startup.AppBasePath;
        }
    }
}
