using System;
using System.Linq;
using Nancy;
using Nancy.ErrorHandling;
using Nancy.Responses.Negotiation;
using Nancy.ViewEngines;
using SourPickle.Models;

namespace SourPickle
{
    public class CustomErrorPageHandler : DefaultViewRenderer, IStatusCodeHandler
    {
        private readonly string _env;
        private readonly Settings _settings;
        
        public CustomErrorPageHandler(IViewFactory factory, Settings settings) : base(factory)
        {
            _env = Environment.GetEnvironmentVariable("SP_ENV");
            _settings = settings;
        }
        
        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound ||
                   statusCode == HttpStatusCode.InternalServerError;
        }
    
        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            Response response = null;
            
            if (IsJsonRequest(context)) {
                return;
            }
            
            dynamic model = new {
                SiteUrl = _settings.SiteUrl,
                Title = _settings.SiteTitle,
            };
            var error = context.Response as ErrorResponse;

            switch (statusCode) {
                case HttpStatusCode.InternalServerError:
                    if (_env != "Production") {
                        model = new {
                            SiteUrl = _settings.SiteUrl,
                            Title = _settings.SiteTitle,
                            ShowError = true,
                            Error = error.ErrorMessage,
                            FullError = error.FullException,
                        };
                    }
            
                    response = RenderView(context, "admin:error.html", model);
                    break;
                default:
                    response = RenderView(context, "admin:notfound.html", model);
                    break;
            }

            response.StatusCode = statusCode;
            context.Response = response;
        }
        
        private static bool IsJsonRequest(NancyContext context)
        {
            var enumerable = context.Request.Headers.Accept;

            var ranges = enumerable.OrderByDescending(o => o.Item2).Select(o => new MediaRange(o.Item1));
            foreach (var item in ranges)
            {
                if (item.Matches("application/json"))
                    return true;
                if (item.Matches("text/json"))
                    return true;
                if (item.Matches("text/html"))
                    return false;
            }

            return true;
        }
    }
}