using System.Collections.Generic;
using System.Threading.Tasks;

namespace SourPickle.Data.Cache
{
    public class InMemoryCache : IDataCache
    {
        private Dictionary<string, object> _cache;
        private object _sync;

        public InMemoryCache()
        {
            _cache = new Dictionary<string, object>();
            _sync = new object();
        }

        public Task<bool> Exists(string key)
        {
            lock (_sync)
            {
                return Task.FromResult(_cache.ContainsKey(key));
            }
        }

        public Task<T> Get<T>(string key) where T : class
        {
            lock (_sync)
            {
                if (!_cache.ContainsKey(key)) {
                    return Task.FromResult<T>(null);
                }
                
                return Task.FromResult<T>((T)_cache[key]);
            }
        }

        public Task<T> Set<T>(string key, T value) where T : class
        {
            lock (_sync)
            {
                if (_cache.ContainsKey(key)) {
                    _cache[key] = value;
                } else {
                    _cache.Add(key, value);
                }
            }
            
            return Task.FromResult(value);
        }

        public Task Remove(string key)
        {
            lock (_sync)
            {
                if (!_cache.ContainsKey(key)) {
                    return Task.FromResult(true);
                }
                
                _cache.Remove(key);
            }

            return Task.FromResult(true);
        }
    }
}