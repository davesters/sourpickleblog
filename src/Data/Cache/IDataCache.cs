using System.Threading.Tasks;

namespace SourPickle.Data.Cache
{
    public interface IDataCache
    {
        /// <summary>
        /// Check if an object exists in cache
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="key">Name of key in cache</param>
        /// <returns>True, if yes; False, otherwise</returns>
        Task<bool> Exists(string key);

        /// <summary>
        /// Get an object from cache
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="key">Name of key in cache</param>
        /// <returns>Object from cache</returns>
        Task<T> Get<T>(string key) where T : class;
        
        /// <summary>
        /// Adds or updates an object to the cache
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="key">Name of key in cache</param>
        /// <param name="value">Object to add to cache</param>
        /// <returns>Object of the class</returns>
        Task<T> Set<T>(string key, T value) where T : class;

        /// <summary>
        /// Remove an object stored with a key from cache
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="key">Key of the object</param>
        Task Remove(string key);
    }
}