using System;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Driver;
using SourPickle.Models;

namespace SourPickle.Data.Mongo
{
    public class MongoDatabase : IDatabase
    {
        private IMongoDatabase _database;
        
        public MongoDatabase(string name)
        {
            var client = new MongoClient(Environment.GetEnvironmentVariable("SP_DATABASEURL"));
            this._database = client.GetDatabase(name);
        }
        
        public void Init()
        {
            var collections = _database.ListCollections().ToList();
            
            if (collections.Count > 0) {
                return;
            }
            
            _database.CreateCollection(typeof(Settings).Name);
            _database.CreateCollection(typeof(Post).Name);
        }
        
        public IDatabaseQuery<T> Query<T>()
        {
            var collection = _database.GetCollection<T>(typeof(T).Name.ToLower());
            
            return new MongoQuery<T>(collection.AsQueryable());
        }
        
        public Task InsertAsync<T>(T entity)
        {
            var collection = _database.GetCollection<T>(typeof(T).Name.ToLower());
            
            return collection.InsertOneAsync(entity);
        }
        
        public async Task<T> UpdateAsync<T>(string id, T entity)
        {
            var collection = _database.GetCollection<T>(typeof(T).Name.ToLower());
            
            var filter = Builders<T>.Filter.Eq("_id", id);
            var options = new UpdateOptions { IsUpsert = true };
            var result = await collection.ReplaceOneAsync(filter, entity, options);
            
            return entity;
        }
    }
}