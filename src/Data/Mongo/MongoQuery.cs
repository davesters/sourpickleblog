using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;

namespace SourPickle.Data.Mongo
{
    public class MongoQuery<T> : IDatabaseQuery<T>
    {
        private IMongoQueryable<T> _query;
        
        public MongoQuery(IMongoQueryable<T> query)
        {
            _query = query;
        }
        
        public IDatabaseQuery<T> Where(Expression<Func<T, bool>> predicate)
        {
            _query = _query.Where(predicate);
            return this;
        }
        
        public IDatabaseQuery<T> OrderBy(Expression<Func<T, string>> selector)
        {
            _query = _query.OrderBy(selector);
            return this;
        }
        
        public IDatabaseQuery<T> OrderByDescending(Expression<Func<T, object>> selector)
        {
            _query = _query.OrderByDescending(selector);
            return this;
        }
        
        public IDatabaseQuery<T> Skip(int skip)
        {
            _query = _query.Skip(skip);
            return this;
        }
        
        public IDatabaseQuery<T> Take(int take)
        {
            _query = _query.Take(take);
            return this;
        }
        
        public Task<int> CountAsync()
        {
            return _query.CountAsync();
        }
        
        public Task<List<T>> ResultAsync()
        {
            return _query.ToListAsync();
        }
    }
}