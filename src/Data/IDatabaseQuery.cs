using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SourPickle.Data
{
    public interface IDatabaseQuery<T>
    {
        IDatabaseQuery<T> Where(Expression<Func<T, bool>> predicate);
        IDatabaseQuery<T> OrderBy(Expression<Func<T, string>> selector);
        IDatabaseQuery<T> OrderByDescending(Expression<Func<T, object>> selector);
        IDatabaseQuery<T> Skip(int skip);
        IDatabaseQuery<T> Take(int take);
        
        Task<int> CountAsync();
        Task<List<T>> ResultAsync();
    }
}