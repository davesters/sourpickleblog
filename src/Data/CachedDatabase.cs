using System.Threading.Tasks;
using SourPickle.Data.Cache;

namespace SourPickle.Data.Mongo
{
    public class CachedDatabase : IDatabase
    {
        private IDataCache _cache;
        private IDatabase _database;
        private static object _sync;
        
        public CachedDatabase(IDataCache cache, IDatabase database)
        {
            this._database = database;
        }
        
        public void Init()
        {
            this._database.Init();
        }
        
        public IDatabaseQuery<T> Query<T>()
        {
            return _database.Query<T>();
        }
        
        public Task InsertAsync<T>(T entity)
        {
            return _database.InsertAsync<T>(entity);
        }
        
        public Task<T> UpdateAsync<T>(string id, T entity)
        {
            return UpdateAsync<T>(id, entity);
        }
    }
}