using System.Threading.Tasks;

namespace SourPickle.Data
{
    public interface IDatabase
    {
        void Init();
        
        IDatabaseQuery<T> Query<T>();
        Task InsertAsync<T>(T entity);
        Task<T> UpdateAsync<T>(string id, T entity);
    }
}