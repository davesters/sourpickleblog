using System;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses;
using Nancy.TinyIoc;
using SourPickle.Data;
using SourPickle.Data.Cache;
using SourPickle.Data.Mongo;
using SourPickle.Models;
using static Nancy.Responses.RedirectResponse;

namespace SourPickle
{ 
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            var cache = new InMemoryCache();
            var database = new MongoDatabase(Environment.GetEnvironmentVariable("SP_DATABASE"));
            database.Init();
            
            container.Register<IDatabase>(database);
            container.Register<IDataCache>(cache).AsSingleton();
            container.Register<Settings>().AsSingleton();
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            this.Conventions.ViewLocationConventions.Clear();
            this.Conventions.ViewLocationConventions.Add((viewName, model, context) =>
            {
                if (viewName.StartsWith("admin:")) {
                    return string.Concat("views/", viewName.Replace("admin:", ""));
                }
                
                return string.Concat("themes/default/views/", viewName);
            });
            
            pipelines.BeforeRequest += Helpers.CheckAuth;
            pipelines.AfterRequest += ctx => {
                if (ctx.Response.StatusCode == HttpStatusCode.Unauthorized) {
                    ctx.Response = new RedirectResponse("/admin/login", RedirectType.SeeOther);
                }
            };
            pipelines.OnError += (ctx, ex) => {
                return ErrorResponse.FromException(ex);
            };
        }

        protected override IRootPathProvider RootPathProvider
        {
            get { return new CustomRootPathProvider(); }
        }
    }
}
