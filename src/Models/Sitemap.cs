using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace SourPickle.Models
{
    public class Sitemap
    {
        internal class SitemapNode
        {
            public DateTime? LastModified { get; set; }
            public string Url { get; set; }
        }

        private string Host { get; set; }
        private IList<SitemapNode> Nodes { get; set; }
        
        public Sitemap(string host)
        {
            Host = host;
            Nodes = new List<SitemapNode>();
        }
        
        public void AddUrl(string url, DateTime updated)
        {
            Nodes.Add(new SitemapNode {
                Url = url,
                LastModified = updated,
            });
        }
        
        public string GetXml()
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");
        
            foreach (SitemapNode node in Nodes)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc", Uri.EscapeUriString($"{Host}/{node.Url}")),
                    node.LastModified == null ? null : new XElement(
                        xmlns + "lastmod", 
                        node.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")));

                root.Add(urlElement);
            }
        
            XDocument document = new XDocument(root);
            return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + document.ToString();
        }
    }
}