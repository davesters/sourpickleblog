using System;
using MongoDB.Bson.Serialization.Attributes;

namespace SourPickle.Models
{
    public class Post
    {
        [BsonId]
        [BsonElement("_id")]
        public string Id { get; set; }
        
        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("slug")]
        public string Slug { get; set; }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("excerpt")]
        public string Excerpt { get; set; }

        [BsonElement("body")]
        public string Body { get; set; }

        [BsonElement("created")]
        public DateTime Created { get; set; }

        [BsonElement("updated")]
        public DateTime Updated { get; set; }

        [BsonElement("published")]
        public DateTime? Published { get; set; }
        
        [BsonElement("isPublished")]
        public bool IsPublished { get; set; }
        
        [BsonElement("views")]
        public int Views { get; set; }

        [BsonElement("tags")]
        public string[] Tags { get; set; }
        
        [BsonElement("inMenu")]
        public bool InMenu { get; set; }
        
        public Post()
        {
            Body = "# Content goes here";
        }
        
        [BsonIgnore]
        public string FriendlyPublishDate {
            get {
                if (!Published.HasValue) {
                    return string.Empty;
                }
                
                return Helpers.GetPrettyDate(Published.Value);
            }
        }
        
        [BsonIgnore]
        public bool IsPage {
            get {
                return Type == "page";
            }
        }
        
        [BsonIgnore]
        public string HtmlBody { get; set; }
    }
}