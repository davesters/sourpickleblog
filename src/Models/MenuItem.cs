
namespace SourPickle.Models
{
    public class MenuItem
    {
        public string Title { get; set; }
        public string Slug { get; set; }
    }
}