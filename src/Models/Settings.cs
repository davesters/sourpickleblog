using System;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SourPickle.Data;

namespace SourPickle.Models
{
    public class Settings
    {
        private IDatabase _database;
        
        [BsonId]
        [BsonElement("_id")]
        public ObjectId Id { get; set; }
        
        [BsonElement("siteTitle")]
        public string SiteTitle { get; set; }

        [BsonElement("siteSubTitle")]
        public string SiteSubTitle { get; set; }

        [BsonElement("siteUrl")]
        public string SiteUrl { get; set; }

        [BsonElement("siteDescription")]
        public string SiteDescription { get; set; }

        [BsonElement("postsPerPage")]
        public int PostsPerPage { get; set; }

        [BsonElement("homeLink")]
        public bool ShowHomeLink { get; set; }

        [BsonElement("saved")]
        public DateTime Saved { get; set; }

        [BsonElement("adminName")]
        public string AdminName { get; set; }

        [BsonElement("adminEmail")]
        public string AdminEmail { get; set; }

        [BsonElement("adminPassword")]
        public string AdminPassword { get; set; }
        
        public Settings() {}
        
        public Settings(IDatabase database)
        {
            this._database = database;
            
            var settings = _database.Query<Settings>()
                .OrderByDescending(s => s.Saved)
                .Take(1)
                .ResultAsync()
                .Result;
            if (settings.Count == 0) {
                settings[0] = new Settings {
                    SiteTitle = "My Blog Site",
                    SiteSubTitle = "Things I wrote",
                    SiteDescription = "",
                    SiteUrl = "http://localhost:5000",
                    PostsPerPage = 5,
                    ShowHomeLink = false,
                    AdminName = "Admin",
                    AdminEmail = "admin@example.com",
                    AdminPassword = "p@ssword",
                };
            }
            
            Update(settings[0]);
        }

        public Task Save()
        {
            Id = new ObjectId();
            
            return _database.InsertAsync<Settings>(this);
        }
        
        public Task Save(Settings settings)
        {
            Update(settings);
            Id = new ObjectId();
            
            return _database.InsertAsync<Settings>(this);
        }
        
        private void Update(Settings settings)
        {
            SiteTitle = settings.SiteTitle;
            SiteSubTitle = settings.SiteSubTitle;
            SiteUrl = settings.SiteUrl;
            SiteDescription = settings.SiteDescription;
            PostsPerPage = settings.PostsPerPage;
            ShowHomeLink = settings.ShowHomeLink;
            AdminName = settings.AdminName;
            AdminEmail = settings.AdminEmail;
            AdminPassword = settings.AdminPassword;
            Saved = DateTime.UtcNow;
        }
    }
}