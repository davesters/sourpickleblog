using System.Collections.Generic;

namespace SourPickle.Models
{
    public class PostsResults
    {
        public List<Post> Posts { get; set; }
        public bool More { get; set; }
        public long Count { get; set; }
        public int Page { get; set; }
    }
}