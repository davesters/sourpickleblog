using System;
using Nancy;
using Newtonsoft.Json;

namespace SourPickle
{
    public class ErrorResponse : Response
    {
        readonly Error error;

        private ErrorResponse(Error error) : base()
        {
            this.error = error;
        }

        public string ErrorMessage
        {
            get { return error.ErrorMessage; }
        }

        public string FullException
        {
            get { return error.FullException; }
        }

        public string[] Errors
        {
            get { return error.Errors; }
        }

        public static ErrorResponse FromMessage(string message)
        {
            return new ErrorResponse(new Error {
                ErrorMessage = message,
            });
        }

        public static ErrorResponse FromException(Exception ex)
        {
            var error = new Error {
                ErrorMessage = ex.Message,
                FullException = ex.ToString(),
            };

            var response = new ErrorResponse(error);
            response.StatusCode = HttpStatusCode.InternalServerError;
            return response;
        }

        public class Error
        {
            public string ErrorMessage { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string FullException { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string[] Errors { get; set; }
        }
    }
}