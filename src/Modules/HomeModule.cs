using System;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;
using Nancy;
using Nancy.Responses;
using Nancy.Responses.Negotiation;
using Nancy.Security;
using NodaTime;
using SourPickle.Data;
using SourPickle.Data.Cache;
using SourPickle.Models;

namespace SourPickle.Modules
{
    public class HomeModule : BaseModule
    {
        public HomeModule(Settings settings, IDatabase database, IDataCache cache)
            : base(settings, database, cache)
        {
            Get["/", true] = async (parameters, ct) => {
                var page = (Request.Query.page != null) ?
                    Convert.ToInt32(Request.Query.page, 10) : 1;
                
                var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
                var query = _database.Query<Post>()
                    .Where(p => p.IsPublished && p.Published <= now && p.Type == "post");
                var count = await query.CountAsync();
                var posts = await query.OrderByDescending(p => p.Published)
                    .Skip((page - 1) * 5)
                    .Take(5)
                    .Result();
                
                dynamic model = new ExpandoObject();
                model.Posts = posts;
                model.PrevPageCheck = page > 1;
                model.PrevPage = page - 1;
                model.NextPageCheck = count > ((page * 5) + 1);
                model.NextPage = page + 1;
                model.Title = "Home";

                return await RenderView("index.html", model);
            };
            
            Get["/robots.txt"] = _ => {
                var robots = $"User-agent: *\nDisallow: /admin/\nSitemap: {_settings.SiteUrl}/sitemap.xml";
                
                return new TextResponse(robots, "text/plain", Encoding.UTF8);
            };
            
            Get["/sitemap", true] = async (parameters, ct) => {
                var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
                var posts = await _database.Query<Post>()
                    .Where(p => p.IsPublished && p.Published <= now && p.Type == "post")
                    .OrderByDescending(p => p.Published)
                    .ResultAsync();
                var sitemap = new Sitemap(_settings.SiteUrl);
                
                posts.ForEach(p => sitemap.AddUrl(p.Slug, p.Updated));
                
                return new TextResponse(sitemap.GetXml(), "text/xml", Encoding.UTF8);
            };
            
            Get["/preview/{slug}", true] = async (parameters, ct) => {
                this.RequiresAuthentication();
                
                var slug = (string)parameters.slug;
                var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
                var posts = await _database.Query<Post>()
                    .Where(p => p.IsPublished && p.Published <= now && p.Slug == slug)
                    .ResultAsync();
                if (posts.Count == 0) {
                    return HttpStatusCode.NotFound;
                }
                
                return await RenderPostView(posts[0]);
            };
            
            Get["/{slug}", true] = async (parameters, ct) => {
                var slug = (string)parameters.slug;
                var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
                var posts = await _database.Query<Post>()
                    .Where(p => p.IsPublished && p.Published <= now && p.Slug == slug)
                    .ResultAsync();
                if (posts.Count == 0) {
                    return HttpStatusCode.NotFound;
                }
                
                return await RenderPostView(posts[0]);
            };
        }
        
        private Task<Negotiator> RenderPostView(Post post)
        {
            dynamic model = new ExpandoObject();
            model.Post = post;
            model.Title = post.Title;
            
            var page = "post.html";
            
            if (post.Type == "page") {
                page = "page.html";
            }

            return RenderView(page, model);
        }
    }
}
