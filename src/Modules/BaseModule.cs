using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Nancy;
using Nancy.Responses.Negotiation;
using SourPickle.Data;
using SourPickle.Data.Cache;
using SourPickle.Models;

namespace SourPickle.Modules
{
    public class BaseModule : NancyModule
    {
        protected Settings _settings;
        protected IDatabase _database;
        protected IDataCache _cache;
        
        public BaseModule(Settings settings, IDatabase database, IDataCache cache)
        {
            _settings = settings;
            _database = database;
            _cache = cache;
        }

        protected Task<Negotiator> RenderView(string view)
        {
            return RenderView(view, new ExpandoObject());
        }
        
        protected async Task<Negotiator> RenderView(string view, dynamic model)
        {
            model.Settings = _settings;
            model.MenuItems = await GetMenuItems();
            
            return View[view, model];
        }
        
        private async Task<IEnumerable<MenuItem>> GetMenuItems()
        {
            var posts = await _database.Query<Post>()
                .Where(p => p.IsPublished && p.InMenu)
                .OrderByDescending(p => p.Published)
                .ResultAsync();
            
            return posts.Select(p => {
                return new MenuItem {
                    Title = p.Title,
                    Slug = p.Slug,
                };
            });
        }
    }
}