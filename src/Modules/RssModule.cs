using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using SourPickle.Models;
using Nancy;
using Terradue.ServiceModel.Syndication;
using SourPickle.Data;
using System.Collections.Generic;
using NodaTime;
using SourPickle.Data.Cache;

namespace SourPickle.Modules
{
    public class RssModule : BaseModule
    {
        public RssModule(Settings settings, IDatabase database, IDataCache cache)
            : base(settings, database, cache)
        {
            Get["/rss", true] = async (parameters, ct) => {
                var rssString = await GetFeed("rss");

                return this.Response
                    .AsText(rssString)
                    .WithContentType("application/rss+xml");
            };

            Get["/atom", true] = async (parameters, ct) => {
                var rssString = await GetFeed("atom");

                return this.Response
                    .AsText(rssString)
                    .WithContentType("application/atom+xml");
            };
        }
        
        private async Task<string> GetFeed(string type) {
            var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
            var posts = await _database.Query<Post>()
                .Where(p => p.IsPublished && p.Published <= now && p.Type == "post")
                .OrderByDescending(p => p.Published)
                .Take(10)
                .ResultAsync();

            var feedFormatter = GetFeedFormatter("rss", posts);
            return GetFeedAsString(feedFormatter);
        }
        
        private SyndicationFeedFormatter GetFeedFormatter(string type, List<Post> posts) {
            var xmlFeed = new SyndicationFeed(_settings.SiteTitle, _settings.SiteDescription, new Uri(_settings.SiteUrl));
            
            xmlFeed.Items = posts.Select(item => {
                return new SyndicationItem(
                    item.Title,
                    item.HtmlBody,
                    new Uri($"{_settings.SiteUrl}/{item.Slug}"),
                    string.Empty,
                    item.Updated);
                });

            if (type == "rss") {
                return new Rss20FeedFormatter(xmlFeed);
            } else {
                return new Atom10FeedFormatter(xmlFeed);
            }
        }
        
        private string GetFeedAsString(SyndicationFeedFormatter formatter) {
            using (var rssString = new StringWriter()) {
                using (var rssWriter = XmlWriter.Create(rssString)) {
                    formatter.WriteTo(rssWriter);
                    
                    return rssString.ToString();
                }
            }
        }
    }
}
