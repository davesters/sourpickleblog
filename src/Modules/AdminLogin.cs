using System;
using System.Dynamic;
using JWT;
using Nancy;
using Nancy.Cookies;
using Nancy.ModelBinding;
using SourPickle.Data;
using SourPickle.Data.Cache;
using SourPickle.Models;
using static Nancy.Responses.RedirectResponse;

namespace SourPickle.Modules
{
    public class AdminLoginModule : BaseModule
    {
        private string _secret = Environment.GetEnvironmentVariable("SP_SECRET");

        public AdminLoginModule(Settings settings, IDatabase database, IDataCache cache)
            : base(settings, database, cache)
        {
            Get["/admin/login", runAsync: true] = async (_, ct) => {
                if (Context.CurrentUser != null) {
                    return Response.AsRedirect("/admin", RedirectType.SeeOther);
                }

                dynamic model = new ExpandoObject();
                model.Title = "Login";
                
                return await RenderView("admin:login.html", model);
            };
            
            Post["/admin/login", runAsync: true] = async (parameters, ct) => {
                var loginModel = this.Bind<Login>();

                if (loginModel.Email != _settings.AdminEmail ||
                    loginModel.Password != _settings.AdminPassword
                ) {
                    dynamic model = new ExpandoObject();
                    model.Title = "Login";
                    model.IsError = true;
                    model.Error = "Invalid login credentials. Try again.";
                    
                    return await RenderView("admin:login.html", model);
                }
                
                var payload = new {
                    user = "admin",
                };
                var token = JsonWebToken.Encode(payload, _secret, JWT.JwtHashAlgorithm.HS256);
                var cookie = new NancyCookie("SP", token);
                
                if (loginModel.RememberMe) {
                    cookie.Expires = DateTime.UtcNow.AddDays(30);
                }
                
                return Response
                    .AsRedirect("/admin")
                    .WithCookie(cookie);
            };
            
            Get["/admin/logout"] = _ => {
                var cookie = new NancyCookie("SP", "", DateTime.UtcNow.AddDays(-1));
                
                return Response
                    .AsRedirect("/admin/login")
                    .WithCookie(cookie);
            };
        }
    }
}
