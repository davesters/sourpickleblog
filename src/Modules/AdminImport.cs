using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CommonMark;
using Nancy;
using Nancy.Security;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;
using SourPickle.Data;
using SourPickle.Data.Cache;
using SourPickle.Models;

namespace SourPickle.Modules
{
    public class AdminImportModule : BaseModule
    {
        public AdminImportModule(Settings settings, IDatabase database, IDataCache cache)
            : base(settings, database, cache)
        {
            this.RequiresAuthentication();

            Post["/admin/import", runAsync: true] = async (parameters, ct) => {
                var file = Request.Files.FirstOrDefault();
                if (file == null) {
                    Console.WriteLine("Uploaded file not found");
                    return Response.AsRedirect("/admin/settings");
                }
                
                JObject json = null;
                using (var reader = new StreamReader(file.Value))
                {
                    json = JObject.Parse(reader.ReadToEnd());
                }
                
                var db = json["db"].FirstOrDefault();
                await SaveSettings((JArray)db["data"]["settings"]);
                await SavePosts((JArray)db["data"]["posts"]);

                return Response.AsRedirect("/admin/settings");
            };
        }
        
        private Task SaveSettings(JArray jsonSettings)
        {
            foreach (var setting in jsonSettings) {
                var key = (string)setting["key"];
                
                switch (key) {
                    case "title":
                        _settings.SiteTitle = (string)setting["value"];
                        continue;
                    case "description":
                        _settings.SiteDescription = (string)setting["value"];
                        continue;
                    case "postsPerPage":
                        _settings.PostsPerPage = (int)setting["value"];
                        continue;
                }
            }
            
            Console.WriteLine("Saving settings");
            Console.WriteLine(JsonConvert.SerializeObject(_settings));
            return _settings.Save();
        }
        
        private Task<Post[]> SavePosts(JArray posts)
        {
            Console.WriteLine(posts.FirstOrDefault().ToString());
            var tasks = new List<Task<Post>>();
            
            try {
                foreach (var post in posts) {
                    var body = (string)post["markdown"];
                    var type = (int)post["page"] == 1 ? "page" : "post";
                    var isPublished = (string)post["status"] == "published";
                    DateTime? publishedDate = null;
                    if (isPublished) {
                        publishedDate = Instant.FromMillisecondsSinceUnixEpoch((long)post["published_at"]).ToDateTimeUtc();
                    }
                    
                    Console.WriteLine("Saving post: " + (string)post["title"]);
                    var id = Guid.NewGuid().ToString().Replace("-", "");
                    tasks.Add(_database.UpdateAsync<Post>(id, new Post {
                        Id = id,
                        Title = (string)post["title"],
                        Slug = (string)post["slug"],
                        Type = type,
                        Excerpt = CommonMarkConverter.Convert(body.Length > 160 ? body.Substring(0, 160) : body),
                        Body = body,
                        HtmlBody = CommonMarkConverter.Convert(body),
                        Created = Instant.FromMillisecondsSinceUnixEpoch((long)post["created_at"]).ToDateTimeUtc(),
                        Updated = Instant.FromMillisecondsSinceUnixEpoch((long)post["updated_at"]).ToDateTimeUtc(),
                        Published = publishedDate,
                        IsPublished = isPublished,
                        Views = 0,
                        InMenu = false,
                    }));
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
            
            return Task.WhenAll(tasks);
        }
    }
}
