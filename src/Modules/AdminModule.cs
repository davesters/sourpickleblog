using System;
using System.Dynamic;
using System.Threading.Tasks;
using CommonMark;
using Nancy;
using Nancy.ModelBinding;
using Nancy.Security;
using NodaTime;
using SourPickle.Data;
using SourPickle.Data.Cache;
using SourPickle.Models;

namespace SourPickle.Modules
{
    public class AdminModule : BaseModule
    {
        public AdminModule(Settings settings, IDatabase database, IDataCache cache)
            : base(settings, database, cache)
        {
            this.RequiresAuthentication();
        
            Get["/admin", runAsync: true] = async (_, ct) => {
                var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
                var posts = await _database.Query<Post>()
                    .Where(p => p.IsPublished && p.Published <= now)
                    .OrderByDescending(p => p.Published)
                    .Take(50)
                    .ResultAsync();
                var drafts = await _database.Query<Post>()
                    .Where(p => !p.IsPublished || p.Published > now)
                    .OrderByDescending(p => p.Created)
                    .Take(50)
                    .ResultAsync();

                posts.InsertRange(0, drafts);
                
                dynamic model = new ExpandoObject();
                model.Posts = posts;
                model.Title = "Content";
                
                return await RenderView("admin:index.html", model);
            };

            Get["/admin/settings", runAsync: true] = async (_, ct) => {
                dynamic model = new ExpandoObject();
                model.Title = "Site Settings";

                return await RenderView("admin:settings.html", model);
            };

            Post["/admin/settings", runAsync: true] = async (_, ct) => {
                var data = this.Bind<Settings>();
                data.Saved = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
                
                await _settings.Save(data);

                dynamic model = new ExpandoObject();
                model.Title = "Site Settings";
                
                return await RenderView("admin:settings.html", model);
            };

            Get["/admin/post/{id?}", runAsync: true] = async (parameters, ct) => {
                dynamic model = new ExpandoObject();
                Post post = new Post();
                post.Type = "post";

                if (parameters.id != null) {
                    var id = (string)parameters.id;
                    var posts = await _database.Query<Post>()
                        .Where(p => p.Id == id)
                        .ResultAsync();
                    if (posts.Count == 0) {
                        return HttpStatusCode.NotFound;
                    }
                    post = posts[0];
                    model.IsDraft = !post.IsPublished;
                }

                model.Post = post;
                model.Title = "Edit Post";

                return await RenderView("admin:edit.html", model);
            };
            
            Post["/admin/post/{id?}", runAsync: true] = async (parameters, ct) => {
                var post = this.Bind<Post>();
                
                post = await SavePost((string)parameters.id, "post", post);

                return Negotiate
                    .WithContentType("application/json")
                    .WithStatusCode(HttpStatusCode.Created)
                    .WithModel(new {
                        success = true,
                        id = post.Id,
                    });
            };

            Get["/admin/page/{id?}", runAsync: true] = async (parameters, ct) => {
                dynamic model = new ExpandoObject();
                Post post = new Post();
                post.Type = "page";

                if (parameters.id != null) {
                    var id = (string)parameters.id;
                    var posts = await _database.Query<Post>()
                        .Where(p => p.Id == id)
                        .ResultAsync();
                    if (posts.Count == 0) {
                        return HttpStatusCode.NotFound;
                    }
                    post = posts[0];
                    model.IsDraft = !post.IsPublished;
                }

                model.Post = post;
                model.Title = "Edit Page";

                return await RenderView("admin:edit.html", model);
            };
            
            Post["/admin/page/{id?}", runAsync: true] = async (parameters, ct) => {
                var post = this.Bind<Post>();
                
                post = await SavePost((string)parameters.id, "page", post);

                return Negotiate
                    .WithContentType("application/json")
                    .WithStatusCode(HttpStatusCode.Created)
                    .WithModel(new {
                        success = true,
                        id = post.Id,
                    });
            };
        }
        
        private async Task<Post> SavePost(string id, string type, Post newPost)
        {
            var post = new Post();
            var now = SystemClock.Instance.GetCurrentInstant().ToDateTimeUtc();
            
            if (!string.IsNullOrWhiteSpace(id)) {
                var posts = await _database.Query<Post>()
                    .Where(p => p.Id == id)
                    .ResultAsync();
                if (posts.Count > 0) {
                    post = posts[0];
                    if (newPost.IsPublished) {
                        post.IsPublished = true;
                    }
                }
            } else {
                post.Id = Guid.NewGuid().ToString().Replace("-", "");
                post.Published = post.Created = now;
                post.IsPublished = newPost.IsPublished;
            }
            
            if (string.IsNullOrWhiteSpace(newPost.Slug)) {
                post.Slug = Helpers.SlugifyText(newPost.Title);
            } else {
                post.Slug = Helpers.SlugifyText(newPost.Slug);
            }
            
            post.Title = newPost.Title;
            post.Type = type;
            post.Body = newPost.Body;
            post.InMenu = newPost.InMenu;
            post.HtmlBody = CommonMarkConverter.Convert(newPost.Body);
            post.Excerpt = CommonMarkConverter.Convert(newPost.Body.Length > 160 ? newPost.Body.Substring(0, 160) : newPost.Body);
            post.Updated = now;

            if (newPost.Published != null) {
                post.Published = newPost.Published;
            }
            
            return await _database.UpdateAsync<Post>(post.Id, post);
        }
    }
}
